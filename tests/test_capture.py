import pytest

from fsforge import FsForgeError, PathElement, iddle_file_processor, reading_file_processor, take_fs_snapshot


def flattenize(bool_alike_test):
    return [(result,) + tapl for result, items_list in bool_alike_test.items() for tapl in items_list]


EQ_TESTS = flattenize({
    True: [
        ("abc", "abc"),
        ("**", "**"),
        ("a*", "a*"),
    ],
    False: [
        ("*", "abc"),
        ("abc", "*"),
    ]
})


@pytest.mark.parametrize("expected_result, arg, other", EQ_TESTS)
def test_path_part_eq(arg, other, expected_result):
    assert bool(PathElement(arg) == PathElement(other)) == expected_result
    assert bool(PathElement(arg) == other) == expected_result


PART_CONTAINS_TESTS = flattenize({
    True: [
        ("abc", "abc"),
        ("abc", "*"),
        ("abc", "a*"),
        ("abc", "a?c"),
        ("abc", "abc"),
        ("file_1.txt", "*"),
        ("file_1.txt", "**"),
        ("file_1.txt", "*.txt"),
    ],
    False: [
        (object(), "abc"),
        (3.14159, "abc"),
        (None, "abc"),
        ("*", "abc"),
        ("abc", "ab"),
        ("ab", "abc"),
        ("a*", "abc"),
        ("file_1.txt", "*.bin"),
    ],
})


@pytest.mark.parametrize("expected_result, other, arg", PART_CONTAINS_TESTS)
def test_path_part_contains(expected_result, other, arg):
    assert bool(other in PathElement(arg)) == expected_result


@pytest.mark.parametrize("expected_result, other, arg", PART_CONTAINS_TESTS)
def test_path_part_contains_other(expected_result, other, arg):
    assert bool(PathElement(other) in PathElement(arg)) == expected_result


def test_capture_fs(example_fs, sample_tree_wo_contents):
    assert take_fs_snapshot("/tmp/ex") == sample_tree_wo_contents


def test_capture_fs_with_reads(example_fs, sample_tree_w_contents):
    masks = {"**": reading_file_processor}
    assert take_fs_snapshot("/tmp/ex", masks) == sample_tree_w_contents

    masks = {"**": {"*": reading_file_processor}}
    assert take_fs_snapshot("/tmp/ex", masks) == sample_tree_w_contents
    masks = {"**": {"*": reading_file_processor}}
    assert take_fs_snapshot("/tmp/ex", masks) == sample_tree_w_contents


def second_byte_reader(file_path):
    with open(file_path, "r") as ff:
        return ord(ff.read()[1])


def test_custom_reading(example_fs):
    masks = {"**": {"*.bin": second_byte_reader}}

    result = take_fs_snapshot("/tmp/ex", masks)

    assert result == {
        'dir_a': {
            'sub_dir_with_a_file': {
            },
            'sub_empty_dir': {
            },
            'file_2.bin': 111
        },
        'dir_b': {
            'f4.bin': 1
        },
        'empty_dir': {
        }
    }


READING_W_MASKS_TESTS = [
    (
        {"empty_dir": {"*.bin": second_byte_reader}},
        {'empty_dir': {}}
    ),
    (
        {"dir_*": {"*.bin": second_byte_reader}},
        {
            'dir_a': {
                'file_2.bin': 111
            },
            'dir_b': {
                'f4.bin': 1
            }
        }
    ),
    (
        {
            "dir_b": {
                "*": reading_file_processor,
            }
        },
        {
            'dir_b': {
                'f4.bin': '\x00\x01\x02\x03\x04\x05\x06\x07\x08\t\n\x0b\x0c',
                'special_file.txt': 'special in dir_b'
            }
        }
    ),
    (
        {
            "**": {
                "*.bin": second_byte_reader,
            },
            "*.txt": reading_file_processor,
        },
        {
            'dir_a': {
                'sub_dir_with_a_file': {},
                'sub_empty_dir': {},
                'file_2.bin': 111
            },
            'dir_b': {'f4.bin': 1},
            'empty_dir': {},
            'special_file.txt': 'so special'
        }
    ),
    (
        {
            "dir_a": {
                "*.bin": second_byte_reader,
                "*": iddle_file_processor,
            },
        },
        {
            'dir_a': {
                'sub_dir_with_a_file': {
                },
                'sub_empty_dir': {
                },
                'file_1.txt': None,
                'file_2.bin': 111,
                'special_file.txt': None,
                'special_file2.txt': None
            }
        }
    ),
    (
        {
            "dir_a": {
                "**": {
                    "*.bin": second_byte_reader,
                    "**": iddle_file_processor,
                },
            },
        },
        {
            'dir_a': {
                'sub_dir_with_a_file': {
                    'special_file.txt': None
                },
                'sub_empty_dir': {
                },
                'file_1.txt': None,
                'file_2.bin': 111,
                'special_file.txt': None,
                'special_file2.txt': None
            }
        }
    ),
]


@pytest.mark.parametrize("mask, expected_result", READING_W_MASKS_TESTS)
def test_reading_with_masks(example_fs, mask, expected_result):
    result = take_fs_snapshot("/tmp/ex", mask)
    assert result == expected_result


def test_raise_with_bad_masks(example_fs):
    mask = {
        "**": {
            "*.bin": second_byte_reader,
            "*?.bin": reading_file_processor,
        },
    }
    with pytest.raises(FsForgeError, match="Unable to distinguish a file processor for a file named: [\w\.]+"):
        take_fs_snapshot("/tmp/ex", mask)


def test_unserializable_path_name():
    class Unserializable(object):
        def __str__(self):
            raise TypeError("Cannot into str.")

    with pytest.raises(TypeError, match="Cannot create PathElement out of Unserializable"):
        PathElement(Unserializable())


def test_path_part_string_conversion():
    pp = PathElement("that")
    assert str(pp) == "that"
    assert repr(pp) == "PathElement('that')"
