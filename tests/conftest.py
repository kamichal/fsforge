import pytest

from fsforge import FileNotRead


@pytest.fixture
def example_fs(fs):
    fs.create_dir('/tmp/ex')
    fs.create_dir('/tmp/ex/dir_a')
    fs.create_dir('/tmp/ex/dir_a/sub_empty_dir')
    fs.create_dir('/tmp/ex/dir_a/sub_dir_with_a_file')
    fs.create_file('/tmp/ex/dir_a/sub_dir_with_a_file/special_file.txt', contents='distinguished contents in a subdir')

    fs.create_file('/tmp/ex/dir_a/file_1.txt', contents='file_1 contents')
    fs.create_file('/tmp/ex/dir_a/file_2.bin', contents='\x63\x6f\x6e\x74\x65\x6e\x74\x73')
    fs.create_file('/tmp/ex/dir_a/special_file.txt', contents='dir_a magic contents')
    fs.create_file('/tmp/ex/dir_a/special_file2.txt', contents='dir_a next magic contents')

    fs.create_dir('/tmp/ex/dir_b')
    fs.create_file('/tmp/ex/dir_b/f4.bin', contents='\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c')
    fs.create_file('/tmp/ex/dir_b/special_file.txt', contents='special in dir_b')

    fs.create_dir('/tmp/ex/empty_dir')
    fs.create_file('/tmp/ex/special_file.txt', contents='so special')


@pytest.fixture
def sample_tree_wo_contents():
    return {
        'dir_a': {
            'sub_empty_dir': {},
            'sub_dir_with_a_file': {
                'special_file.txt': FileNotRead
            },
            'file_1.txt': FileNotRead,
            'file_2.bin': FileNotRead,
            'special_file.txt': FileNotRead,
            'special_file2.txt': FileNotRead,
        },
        'dir_b': {
            'f4.bin': FileNotRead,
            'special_file.txt': FileNotRead,
        },
        'empty_dir': {},
        'special_file.txt': FileNotRead,
    }


@pytest.fixture
def sample_tree_w_contents():
    return {
        'dir_a': {
            'sub_dir_with_a_file': {
                'special_file.txt': 'distinguished contents in a subdir'
            },
            'sub_empty_dir': {},
            'file_1.txt': 'file_1 contents',
            'file_2.bin': 'contents',
            'special_file.txt': 'dir_a magic contents',
            'special_file2.txt': 'dir_a next magic contents',
        },
        'dir_b': {
            'f4.bin': '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c',
            'special_file.txt': 'special in dir_b',
        },
        'empty_dir': {},
        'special_file.txt': 'so special'
    }


@pytest.fixture
def expected_pretty_repr():
    return r"""{
    'dir_a': {
        'sub_dir_with_a_file': {
            'special_file.txt': 'distinguished contents in a subdir'
        },
        'sub_empty_dir': {
        },
        'file_1.txt': 'file_1 contents',
        'file_2.bin': 'contents',
        'special_file.txt': 'dir_a magic contents',
        'special_file2.txt': 'dir_a next magic contents'
    },
    'dir_b': {
        'f4.bin': '\x00\x01\x02\x03\x04\x05\x06\x07\x08\t\n\x0b\x0c',
        'special_file.txt': 'special in dir_b'
    },
    'empty_dir': {
    },
    'special_file.txt': 'so special'
}
"""


@pytest.fixture
def sample_walk_result():
    return {
        '/tmp/ex': (
            ['dir_a', 'dir_b', 'empty_dir'],
            ['special_file.txt'],
        ),
        '/tmp/ex/dir_a/sub_dir_with_a_file': ([], ['special_file.txt']),
        '/tmp/ex/dir_a/sub_empty_dir': ([], []),
        '/tmp/ex/dir_b': ([], ['f4.bin', 'special_file.txt']),
        '/tmp/ex/dir_a': (
            ['sub_dir_with_a_file', 'sub_empty_dir'],
            ['file_1.txt', 'file_2.bin', 'special_file.txt', 'special_file2.txt']
        ),
        '/tmp/ex/empty_dir': ([], [])
    }
