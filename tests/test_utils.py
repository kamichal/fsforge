import shutil
import sys
from tempfile import mkdtemp

import pytest

from fsforge import RealFS, create_fs, flatten_fs_tree
from fsforge._utils import os


def test_flatten(mocker):
    mocker.patch.object(os.path, "join", side_effect=lambda a, b: "{}/{}".format(a, b) if a else b)

    inp = {
        'FOO': {
            'ni': {
                'knight': {
                    '9_24184': {
                        'someTD.json': '{"some_number": 5}',
                        'cell0.bin': 50,
                        'cell1.bin': 50
                    }
                }
            },
            'bar': {
                'one': 'thing.txt'
            },
            'baz': {},
            'headers.txt': 20000,
        },
        'all_messages.txt': 4000,
    }

    assert flatten_fs_tree(inp) == {
        'FOO/bar/one': 'thing.txt',
        'FOO/headers.txt': 20000,
        'FOO/ni/knight/9_24184/cell0.bin': 50,
        'FOO/ni/knight/9_24184/cell1.bin': 50,
        'FOO/ni/knight/9_24184/someTD.json': '{"some_number": 5}',
        'all_messages.txt': 4000,
    }


class TestRealFs():

    @pytest.fixture(autouse=True)
    def setup(self, mocker):
        self.mkdirs_mock = mocker.patch.object(os, "makedirs")
        self.chmod_mock = mocker.patch.object(os, "chmod")
        self.write_mock = mocker.patch.object(RealFS, "_write_file")
        self.tmp_dir = mkdtemp()
        yield
        shutil.rmtree(self.tmp_dir)

    def test_empty_file_created(self):
        fs_tree = {
            'FOO': {
                'one.txt': ""
            },
        }

        create_fs(RealFS, fs_tree, self.tmp_dir)
        self.mkdirs_mock.assert_called_with(os.path.join(self.tmp_dir, 'FOO'))
        self.chmod_mock.assert_called_once_with(os.path.join(self.tmp_dir, 'FOO', 'one.txt'), 0o666)
        mode = 'w' if sys.version_info[0] > 2 else 'wb'
        self.write_mock.assert_called_once_with('', os.path.join(self.tmp_dir, 'FOO', 'one.txt'), mode)

    def test_bin_file_created(self):
        fs_tree = {
            'FOO': {
                'a.bin': "\x01\x07"
            },
        }
        create_fs(RealFS, fs_tree, self.tmp_dir)
        self.mkdirs_mock.assert_called_with(os.path.join(self.tmp_dir, 'FOO'))
        self.chmod_mock.assert_called_once_with(os.path.join(self.tmp_dir, 'FOO', 'a.bin'), 0o666)
        mode = 'w' if sys.version_info[0] > 2 else 'wb'
        self.write_mock.assert_called_once_with('\x01\x07', os.path.join(self.tmp_dir, 'FOO', 'a.bin'), mode)

    @pytest.mark.parametrize('size', [0, 12])
    def test_file_created_by_size(self, size):
        fs_tree = {
            'sized.bin': size

        }
        create_fs(RealFS, fs_tree, self.tmp_dir)
        self.mkdirs_mock.assert_not_called()
        self.chmod_mock.assert_called_once_with(os.path.join(self.tmp_dir, 'sized.bin'), 0o666)
        mode = 'wb' if sys.version_info[0] > 2 else 'w'
        self.write_mock.assert_called_once_with('\x00' * size, os.path.join(self.tmp_dir, 'sized.bin'), mode)
