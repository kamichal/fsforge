import os

from fsforge import create_fs, nicer_fs_repr, reading_file_processor, take_fs_snapshot


def test_printing_fs(sample_tree_w_contents, expected_pretty_repr):
    assert nicer_fs_repr(sample_tree_w_contents) == expected_pretty_repr


def comparable_os_walk(path_to_walk):
    return {root: (sorted(dirs), sorted(files)) for root, dirs, files in os.walk(path_to_walk)}


def test_recreating_fake_fs(fs, sample_tree_wo_contents, sample_tree_w_contents, sample_walk_result):
    mount_point = "/tmp/ex"
    create_fs(fs, sample_tree_w_contents, mount_point)

    assert comparable_os_walk(mount_point) == sample_walk_result
    masks = {"**": reading_file_processor}
    assert take_fs_snapshot(mount_point, masks) == sample_tree_w_contents
    assert take_fs_snapshot(mount_point) == sample_tree_wo_contents
